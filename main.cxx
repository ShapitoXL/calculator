#include <iostream>
#include <string>
#include <vector>
#include <cstdint>
enum LexemType
{
    LNumber, //0
    LPlus,
    LMinus,
    LMult,
    LDiv,
    LBracketOpen,
    LBracketClose //6
};

struct lexem
{
    LexemType type;
    double value = 0;
};

void getAllLexem(const std::string &str, std::vector<lexem> &vec)
{
    for (int i = 0; i < str.size(); i++)
    {
        lexem temp;
        if (str[i] == '+')
        {
            temp.type = LPlus;
        }
        else if (str[i] == '-')
        {
            temp.type = LMinus;
        }
        else if (str[i] == '*')
        {
            temp.type = LMult;
        }
        else if (str[i] == '/')
        {
            temp.type = LDiv;
        }
        else if (str[i] == '(')
        {
            temp.type = LBracketOpen;
        }
        else if (str[i] == ')')
        {
            temp.type = LBracketClose;
        }
        else if (isdigit(str[i]))
        {
            temp.type = LNumber;
            temp.value = (str[i] - '0');
            for (int j = i + 1; j < str.size(); j++)
            {
                if (isdigit(str[j]))
                {
                    temp.value = temp.value * 10 + (str[j] - '0');
                    i++;
                }
                else
                {
                    break;
                }
            }
        }
        else
        {
            std::cerr << "Unknown symbol '" << str[i] << "' in expression" << std::endl;
            exit(1);
        }
        vec.push_back(temp);
    }
}

double calculate(std::vector<lexem> &vec)
{
    //check brackets
    int bracketsOpen = 0;
    int bracketsClose = 0;
    for (int i = 0; i < vec.size(); i += 1)
    {
        if (vec[i].type == LBracketOpen)
        {
            bracketsOpen++;
        }
        else if (vec[i].type == LBracketClose)
        {
            bracketsClose++;
        }
        if (bracketsClose > bracketsOpen)
        {
            std::cerr << "You missing brackets" << std::endl;
            exit(1);
        }
    }
    if (bracketsOpen != bracketsClose)
    {
        std::cerr << "You missing brackets" << std::endl;
        exit(1);
    }
    //catch brackets
    if (bracketsOpen > 0)
    {
        int begin = 0, end = 0;
        bool again = true;
        while (again)
        {
            again = false;
            bracketsOpen = 0;
            bracketsClose = 0;
            begin = 0;
            for (int i = begin; i < vec.size(); i++)
            {
                if (vec[i].type == LBracketOpen)
                {
                    begin = i;
                    break;
                }
            }
            bracketsOpen++;
            end = begin + 1;
            for (int i = begin + 1; i < vec.size(); i++)
            {
                if (vec[i].type == LBracketOpen)
                {
                    bracketsOpen++;
                }
                if (vec[i].type == LBracketClose)
                {
                    bracketsClose++;
                    if (bracketsClose == bracketsOpen)
                    {
                        end = i;
                        break;
                    }
                }
            }
            std::vector<lexem> newVector;
            for (int i = begin + 1; i < end; i++)
            {
                lexem temp;
                temp = vec[i];
                newVector.push_back(temp);
            }
            lexem newValue;
            newValue.type = LNumber;
            newValue.value = calculate(newVector);
            vec.erase(vec.begin() + begin, vec.begin() + end + 1);
            vec.insert(vec.begin() + begin, newValue);
            for (int i = 0; i < vec.size(); i++)
            {
                if (vec[i].type == LBracketOpen)
                {
                    again = true;
                    break;
                }
            }
        }
    }
    //multiply and division
    for (int i = 0; i < vec.size() - 1; i++)
    {
        lexem result;
        result.type = LNumber;
        if (vec[i].type == LMult)
        {
            //multiply
            result.value = vec[i - 1].value * vec[i + 1].value;
            vec.erase(vec.begin() + i - 1, vec.begin() + i + 2);
            vec.insert(vec.begin() + i - 1, result);
            i = -1;
        }
        else if (vec[i].type == LDiv)
        {
            //division
            result.value = vec[i - 1].value / vec[i + 1].value;
            vec.erase(vec.begin() + i - 1, vec.begin() + i + 2);
            vec.insert(vec.begin() + i - 1, result);
            i = -1;
        }
    }
    //plus and minus
    double result = vec[0].value;
    for (int i = 1; i < vec.size() - 1; i += 1)
    {
        if (vec[i].type == LPlus)
        {
            //plus
            result += vec[i + 1].value;
        }
        else if (vec[i].type == LMinus)
        {
            //minus
            result -= vec[i + 1].value;
        }
    }
    return result;
}

int main()
{
    std::cout << "This ia a simply calculator which can add, subtract, multiply and divide numbers.\n\n";
    std::string expression = "";
    while (true)
    {
        std::cout << "Use \"help\" to see instruction or enter an expression: ";
        std::getline(std::cin, expression);
        if (expression == "\"help\"")
        {
            std::cout << std::endl
                      << "Without quotes :))" << std::endl
                      << std::endl;
        }
        else if (expression == "help")
        {
            std::cout << "\nThis calculator can add(+), subtract(-), multiply(*) and divide(/) integer number. In expression you shouldn't use spaces or other symbols except 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, +, -, *, /, (, ). You must use only binary operators.\nIn brackets must be at least one number\n"
                      << std::endl;
        }
        else
        {
            try
            {
                std::vector<lexem> lexems;
                getAllLexem(expression, lexems);
                double result = calculate(lexems);
                std::cout << "Result: " << result << std::endl;
            }
            catch (...)
            {
                std::cerr << "Unknown error" << std::endl;
            }
        }
    }
    return 0;
}
